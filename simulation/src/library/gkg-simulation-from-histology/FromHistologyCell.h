#ifndef _gkg_simulation_from_histology_FromHistologyCell_h_
#define _gkg_simulation_from_histology_FromHistologyCell_h_


#include <gkg-simulation-virtual-tissue/Cell.h>
#include <gkg-processing-container/BoundingBox.h>
#include <gkg-processing-container/MeshMap.h>



namespace gkg
{


class FromHistologyPopulation;


class FromHistologyCell : public Cell
{

  public:

    FromHistologyCell( int32_t id,
           const FromHistologyPopulation* parent,
           const MeshMap< int32_t, float, 3U >& cellMeshMap,
           uint8_t atomType );
    FromHistologyCell( const FromHistologyCell& other );
    virtual ~FromHistologyCell();

    FromHistologyCell& operator=( const FromHistologyCell& other );

    const BoundingBox< float >& getFieldOfView() const;


  protected:

    void computeSphereAtomDecompositionFromMeshMap();
    void computeEllipoidAtomDecompositionFromSphereAtomDecomposition();

    BoundingBox< float > _fieldOfView;


};
   

}


#endif

