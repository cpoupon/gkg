#include <gkg-simulation-from-histology/FromHistologyCell.h>
#include <gkg-simulation-from-histology/FromHistologyPopulation.h>
#include <gkg-simulation-virtual-tissue/SphereAtom.h>
#include <gkg-simulation-virtual-tissue/EllipsoidAtom.h>
#include <gkg-processing-numericalanalysis/NumericalAnalysisSelector.h>
#include <gkg-processing-container/MeshMap_i.h>
#include <gkg-core-exception/Exception.h>



gkg::FromHistologyCell::FromHistologyCell(
                           int32_t id,
                           const FromHistologyPopulation* parent,
                           const gkg::MeshMap< int32_t, float, 3U >& cellMeshMap,
                           uint8_t atomType )
                       : gkg::Cell( id, parent )
{

  try
  {

    /////////////////////////////////////////////////////////////////////////////
    // collecting field of view
    /////////////////////////////////////////////////////////////////////////////

    _fieldOfView = cellMeshMap.vertices.getBoundingBox( 0 );


    /////////////////////////////////////////////////////////////////////////////
    // decomposing mesh into atom(s)
    /////////////////////////////////////////////////////////////////////////////

    computeSphereAtomDecompositionFromMeshMap();

    if ( atomType == gkg::EllipsoidAtom::getStaticType() )
    {

      computeEllipoidAtomDecompositionFromSphereAtomDecomposition();

    }


  }
  GKG_CATCH( "gkg::FromHistologyCell::FromHistologyCell( "
             "int32_t id, "
             "const FromHistologyPopulation* parent, "
             "const gkg::MeshMap< int32_t, float, 3U >& cellMeshMap )" );


}


gkg::FromHistologyCell::FromHistologyCell( const gkg::FromHistologyCell& other )
                       : gkg::Cell( other ),
                         _fieldOfView( other._fieldOfView )
{


}


gkg::FromHistologyCell::~FromHistologyCell()
{
}


gkg::FromHistologyCell& 
gkg::FromHistologyCell::operator=( const gkg::FromHistologyCell& other )
{

  try
  {

    this->gkg::Cell::operator=( other );
    _fieldOfView = other._fieldOfView;

    return *this;

  }
  GKG_CATCH( "gkg::FromHistologyCell& "
             "gkg::FromHistologyCell::operator=( "
             "const gkg::FromHistologyCell& other )" );

}



const gkg::BoundingBox< float >& gkg::FromHistologyCell::getFieldOfView() const
{

  try
  {

    return _fieldOfView;

  }
  GKG_CATCH( "const gkg::BoundingBox< float >& "
             "gkg::FromHistologyCell::getFieldOfView() const" );

}


void gkg::FromHistologyCell::computeSphereAtomDecompositionFromMeshMap()
{

  try
  {

    throw std::runtime_error( "not implemented yet" );

  }
  GKG_CATCH( "void gkg::FromHistologyCell::"
             "computeSphereAtomDecompositionFromMeshMap()" );

}


void gkg::FromHistologyCell::
                    computeEllipoidAtomDecompositionFromSphereAtomDecomposition()
{

  try
  {

    throw std::runtime_error( "not implemented yet" );

  }
  GKG_CATCH( "void gkg::FromHistologyCell::"
             "computeEllipoidAtomDecompositionFromSphereAtomDecomposition()" );

}
