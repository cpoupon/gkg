#include <gkg-simulation-from-histology/FromHistologyPopulation.h>
#include <gkg-simulation-virtual-tissue/PopulationFactory.h>
#include <gkg-simulation-from-histology/FromHistologyCell.h>
#include <gkg-simulation-virtual-tissue/VirtualTissue.h>
#include <gkg-simulation-virtual-tissue/SphereAtom.h>
#include <gkg-simulation-virtual-tissue/EllipsoidAtom.h>
#include <gkg-processing-container/MeshMap_i.h>
#include <gkg-core-pattern/RCPointer.h>
#include <gkg-core-io/Reader_i.h>
#include <gkg-core-exception/Exception.h>



gkg::FromHistologyPopulation::FromHistologyPopulation(
                                               int32_t id,
                                               const gkg::VirtualTissue* parent,
                                               const gkg::Dictionary& parameters,
                                               bool /*verbose*/,
                                               std::ofstream* /*osLog*/ )
                             : gkg::Population( id, parent )
{

  try
  {

    ////////////////////////////////////////////////////////////////////////////
    // collecting file type (MeshMap or Volume)
    ////////////////////////////////////////////////////////////////////////////

    std::string fileType = gkg::getStringValue( parameters,
                                                "file_type",
                                                "" );

    ////////////////////////////////////////////////////////////////////////////
    // collecting atom type for decomposition (sphere-atom or allipsoid-atom)
    ////////////////////////////////////////////////////////////////////////////

    std::string stringAtomType = gkg::getStringValue( parameters,
                                                      "atom_type",
                                                      true,
                                                      "" );
    uint8_t atomType = 0U;
    if ( stringAtomType == "sphere-atom" )
    {
    
      atomType = gkg::SphereAtom::getStaticType();

    }
    else if (  stringAtomType == "ellipsoid-atom" )
    {
    
      atomType = gkg::EllipsoidAtom::getStaticType();

    }
    else
    {

      throw std::runtime_error( "unknown atom type" );

    }

    ////////////////////////////////////////////////////////////////////////////
    // using the adequate reading method according to the file type
    ////////////////////////////////////////////////////////////////////////////

    if ( fileType == "MeshMap" )
    {

      // collecting cell mesh map filenames
      std::vector< std::string > 
        cellMeshMapFileNames = gkg::getVectorOfStringValue( 
                                                       parameters,
                                                       "cell_mesh_map_filenames",
                                                       0 );

      // reading mesh maps and creating cells on the fly
      this->readMeshMapsAndCreateCells( cellMeshMapFileNames, atomType );

    }
    else if ( fileType == "Volume" )
    {

      // collecting cell volume filename
      std::string
        cellVolumeFileName = gkg::getStringValue( parameters,
                                                  "cell_volume_filename",
                                                  true,
                                                  "" );
      this->readVolumeAndCreateCells( cellVolumeFileName, atomType );

    }

  }
  GKG_CATCH( "gkg::FromHistologyPopulation::FromHistologyPopulation( "
             "int32_t id, "
             "const gkg::VirtualTissue* parent, "
             "const gkg::Dictionary& parameters, "
             "bool verbose, "
             "std::ofstream* osLog )" );

}



gkg::FromHistologyPopulation::FromHistologyPopulation(
                                      const gkg::FromHistologyPopulation& other )
                             : gkg::Population( other ),
                               _fieldOfView( other._fieldOfView )
{
}


gkg::FromHistologyPopulation::~FromHistologyPopulation()
{
}



gkg::FromHistologyPopulation& 
gkg::FromHistologyPopulation::operator=(
                                      const gkg::FromHistologyPopulation& other )
{

  try
  {

    this->gkg::Population::operator=( other );
    _fieldOfView = other._fieldOfView;

    return *this;

  }
  GKG_CATCH( "gkg::FromHistologyPopulation& "
             "gkg::FromHistologyPopulation::operator=( "
             "const gkg::FromHistologyPopulation& other )" );

}


std::string gkg::FromHistologyPopulation::getTypeName() const
{

  try
  {

    return getStaticName();

  }
  GKG_CATCH( "std::string gkg::FromHistologyPopulation::getTypeName() const" );

}


const gkg::BoundingBox< float >& 
gkg::FromHistologyPopulation::getFieldOfView() const
{

  try
  {

    return _fieldOfView;

  }
  GKG_CATCH( "const gkg::BoundingBox< float >& "
             "gkg::FromHistologyPopulation::getFieldOfView() const" );

}


std::string gkg::FromHistologyPopulation::getStaticName()
{

  try
  {

    return "from-histology-population";

  }
  GKG_CATCH( "std::string gkg::FromHistologyPopulation::getStaticName()" );

}


void gkg::FromHistologyPopulation::readMeshMapsAndCreateCells(
                          const std::vector< std::string >& cellMeshMapFileNames,
                          uint8_t atomType )
{

  try
  {

    float lowerX = 1e38f;
    float lowerY = 1e38f;
    float lowerZ = 1e38f;

    float upperX = -1e38f;
    float upperY = -1e38f;
    float upperZ = -1e38f;

    int32_t fromHistologyCellId = 0;

    std::list< gkg::RCPointer< gkg::FromHistologyCell > > fromHistologyCells;

    std::vector< std::string >::const_iterator 
      f = cellMeshMapFileNames.begin(),
      fe = cellMeshMapFileNames.end();
    while ( f != fe )
    {

      gkg::MeshMap< int32_t, float, 3U > cellMeshMap;
      gkg::Reader::getInstance().read( *f, cellMeshMap );

      gkg::RCPointer< gkg::FromHistologyCell >
        fromHistologyCell( new gkg::FromHistologyCell( fromHistologyCellId,
                                                       this,
                                                       cellMeshMap,
                                                       atomType ) );

      fromHistologyCells.push_back( fromHistologyCell );

      const gkg::BoundingBox< float >&
        cellFieldOfView = fromHistologyCell->getFieldOfView();

      if ( cellFieldOfView.getLowerX() < lowerX )
      {

        lowerX = cellFieldOfView.getLowerX();

      }
      if ( cellFieldOfView.getLowerY() < lowerY )
      {

        lowerY = cellFieldOfView.getLowerY();

      }
      if ( cellFieldOfView.getLowerZ() < lowerZ )
      {

        lowerZ = cellFieldOfView.getLowerZ();

      }
      if ( cellFieldOfView.getUpperX() > upperX )
      {

        upperX = cellFieldOfView.getUpperX();

      }
      if ( cellFieldOfView.getUpperY() > upperY )
      {

        upperY = cellFieldOfView.getUpperY();

      }
      if ( cellFieldOfView.getUpperZ() > upperZ )
      {

        upperZ = cellFieldOfView.getUpperZ();

      }

      ++ fromHistologyCellId;
      ++ f;

    }


    /////////////////////////////////////////////////////////////////////////////
    // storing the population field of view
    /////////////////////////////////////////////////////////////////////////////

    _fieldOfView.setLowerX( lowerX );
    _fieldOfView.setLowerY( lowerY );
    _fieldOfView.setLowerZ( lowerZ );

    _fieldOfView.setUpperX( upperX );
    _fieldOfView.setUpperY( upperY );
    _fieldOfView.setUpperZ( upperZ );


    ////////////////////////////////////////////////////////////////////////////
    // copying back fibers to protected member(s)
    ////////////////////////////////////////////////////////////////////////////

    _cells = std::vector< gkg::RCPointer< gkg::Cell > >(
                                                      fromHistologyCells.begin(),
                                                      fromHistologyCells.end() );

  }
  GKG_CATCH( "void gkg::FromHistologyPopulation::readMeshMapsAndCreateCells( "
             "const std::vector< std::string >& cellMeshMapFileNames, "
             "uint8_t atomType )" );

}


void gkg::FromHistologyPopulation::readVolumeAndCreateCells(
                                      const std::string & /*cellVolumeFileName*/,
                                      uint8_t /*atomType*/ )
{

  try
  {

    throw std::runtime_error( "not implemented yet" );


  }
  GKG_CATCH( "void gkg::FromHistologyPopulation::readVolumeAndCreateCells( "
             "const std::string & cellVolumeFileName, "
             "uint8_t atomType )" );

}




//
// adding the FiberPopulation creator to the population factory
//

RegisterPopulationCreator( gkg, FromHistologyPopulation );


