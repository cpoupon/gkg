#ifndef _gkg_simulation_from_histology_FromHistologyPopulation_h_
#define _gkg_simulation_from_histology_FromHistologyPopulation_h_


#include <gkg-simulation-virtual-tissue/Population.h>
#include <gkg-processing-container/BoundingBox.h>
#include <gkg-core-object/Dictionary.h>
#include <gkg-core-pattern/Creator.h>
#include <vector>
#include <string>


namespace gkg
{


class FromHistologyPopulation : public Population,
                                public Creator5Arg< FromHistologyPopulation,
                                                    Population,
                                                    int32_t,
                                                    const VirtualTissue*,
                                                    const Dictionary&,
                                                    bool,
                                                    std::ofstream* >
{

  public:

    FromHistologyPopulation( const FromHistologyPopulation& other );
    virtual ~FromHistologyPopulation();

    FromHistologyPopulation& operator=( const FromHistologyPopulation& other );

    std::string getTypeName() const; 

    const BoundingBox< float >& getFieldOfView() const;

    static std::string getStaticName();

  protected:


    friend struct Creator5Arg< FromHistologyPopulation,
                               Population,
                               int32_t,
                               const VirtualTissue*,
                               const Dictionary&,
                               bool,
                               std::ofstream* >;

    FromHistologyPopulation( int32_t id,
                             const VirtualTissue* parent,
                             const Dictionary& parameters,
                             bool verbose,
                             std::ofstream* osLog );

    void readMeshMapsAndCreateCells(
                          const std::vector< std::string >& cellMeshMapFileNames,
                          uint8_t atomType );

    void readVolumeAndCreateCells(
                          const std::string & cellVolumeFileName,
                          uint8_t atomType );

    BoundingBox< float > _fieldOfView;

};


}


#endif
